<?php

use Nette\Diagnostics\Debugger;
/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{
	private $genreList	= array('classical' => 'classical.xml', 
								'countryfolk' => 'country-folk.xml', 
								'electronic' => 'electronic.xml', 
								'hiphop' => 'hiphop.xml', 
								'pop' => 'pop.xml', 
								'rock' => 'rock.xml');
	
	private $genreDir	= "/xml/";
	
	public function renderDefault()
	{               
            
           // $this->getGenreData();
	}
	
	public function renderGraphs()
	{   
   //	return;               
   		$data = $this->getGenreData();
   
		$graph = new Graph(600, 600, 0, 128 * 1024 * 1024);
		                          
        $graphDir = $this->context->params['wwwDir'] . '/graphs/';		                              
		if (!file_exists($graphDir)) {
			mkdir($graphDir, 0777);                       
		}									  									  		                              
		                                                   
		foreach ($data as $gkey => $genre) {
			$genreName = $graphDir .  $gkey . '/';
			if (!file_exists($genreName)) {
				mkdir($genreName, 0777);                       
			}	
			foreach($genre as $key => $descriptors) { 
				$path = $genreName . $key . '.png'; 
				$images[$gkey][$key] =  $gkey . '/' . $key . '.png';
				
				if (file_exists($path))
					continue;
					
				foreach($descriptors as $a) {
					if (is_numeric($a)) {
				 		$a = $a * 1000000;	
						$graph->add($a);
					}
				}            
				$graph->draw($path);
				       
			}                     
		}
		
		$this->template->graphs = $images;
	}
	
	public function createComponentUploadForm() {
		$form = new \Nette\Application\UI\Form();
		
		$form->addUpload('file', 'Skladba')
			->setRequired('Nejprve vyberte skladbu.');
			
		$form->addRadioList('type', 'Typ analýzy', array('Zjisti žánr', 'Ukaž míru podobnosti'))
			->setDefaultValue(0);	
			
		$form->addSubmit('send', 'Analyzovat');
		
		$form->onSuccess[] = $this->uploadComplete;
		
		return $form;
	}
	
	public function uploadComplete($form) {
		$values = $form->getValues();
		$file = $values['file'];
		
		if ($file->isOk()) {
			$ar = explode('.', $file->getSanitizedName());
            $name = $ar[0];
            $ext = end($ar);   
            $uniqName = \Nette\Utils\Strings::random(30) . '.' . strtolower($ext);
                
            $origPath = $this->presenter->context->params['wwwDir'] . '/upload/files/' . $uniqName; 
            $file->move($origPath);
            
            $do = "java -jar \"".$this->context->params['wwwDir']."/jAudio/jAudio.jar\" -s \"".$origPath."\"";
            echo $do;
            
            echo "<br>";
            echo "<br>";
            echo "<br>";
            echo "<br>";
            echo "<br>";
            
            exec($do, $output);
            var_dump($output) ;
            dd($output);
            
            exit;
            //TODO save to database
            
            // ZPracování skladby
            /*
             <?php exec("java -jar jAudio-1.0.4.jar -s settings.xml ./output
			> ./sound.wav"); ?>
			>
			> nebo takle:
			>
			> <?php exec("java -jar jAudio-1.0.4.jar -s settings.xml ./sound.wav",
			> $output); ?>
			*/
            
		}   		
	}
	private function getGenreData() {
		return $this->loadGenreXML();
	}
	
	private function loadGenreXML() {
		$genresData = new Nette\ArrayHash();
		$genreXML = NULL;
		try {	
			foreach ($this->genreList as $genreName => $genreFile) {
				$genreXML = simplexml_load_file($this->context->params['wwwDir'] . $this->genreDir . $genreFile);
				$songCount = 0;
				foreach ($genreXML->data_set as $songData) {
					$decsriptorCount = 0;
					$songId = 'song' . $songCount;
					foreach ($songData->feature as $descriptor) {
						$descId = 'desc' . $decsriptorCount;
						if($descriptor->v->count()==1) {
							$genresData->{$genreName}[$songId][$descId] = (float) $descriptor->v;
						} else {
							$descriptorSubArray = array();
							foreach($descriptor->v as $descriptorSubElement){
								$descriptorSubArray[] = (float) $descriptorSubElement;
							}
							$genresData->{$genreName}[$songId][$descId] = $descriptorSubArray;
						}
						$decsriptorCount++;
					}
					$songCount ++;
				}				
			}
                    
			return $genresData;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

}

                    /*
			foreach ($this->genreList as $genreName => $genreFile) {
				$genreXML = simplexml_load_file($this->context->params['wwwDir'] . $this->genreDir . $genreFile);
				$songCount = 0;
				foreach ($genreXML->data_set as $songData) {
					$decsriptorCount = 0;
					$songId = 'song' . $songCount;
					foreach ($songData->feature as $descriptor) {
						$descId = 'desc' . $decsriptorCount;
						if($descriptor->v->count()==1) {
							$genresData->{$genreName}[$descId][] = (float) $descriptor->v;
						} else {
							$descriptorSubArray = array();
							foreach($descriptor->v as $descriptorSubElement){
								$descriptorSubArray[] = (float) $descriptorSubElement;
							}
							$genresData->{$genreName}[$descId][] = $descriptorSubArray;
						}
						$decsriptorCount++;
					}
					$songCount ++;
				}				
			}
                     * 
                     */
