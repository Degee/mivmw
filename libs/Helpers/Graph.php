<?php

use Nette\Image as NImage,
	Nette\Object as NObject;


class Graph extends NObject
{

	protected $width;
	protected $height;
	protected $min;
	protected $max;

	protected $values = array();


	public function __construct($width, $height, $min, $max)
	{
		$this->width = (int) $width;
		$this->height = (int) $height;
		$this->min = (int) $min;
		$this->max = (int) $max;
	}


	public function add($value) {
		$this->values[] = $value;
	}


	public function draw($path = NULL) {
		$image = NImage::fromBlank($this->width, $this->height, NImage::rgb(0xff,0xff,0xff));
		$image->line(0, $this->height >> 1, $this->width, $this->height >> 1, NImage::rgb(0,0,0));
		$count = count($this->values);

		$last = $peak = NULL;
		$lastD = ($this->height >> 1);
		for ($i = 0; $i < $count; $i++) {
			$x = (int) round($i / ($count-1) * $this->width);
			$y = $this->height;
			
			$tmp = $this->values[$i];
			$tmp -= $this->min;
			$y = $y - $tmp * $this->height / ($this->max - $this->min);
			if (isset($last)) {
				$d = ($this->height >> 1) - ($this->values[$i] - $last) * $this->height / ($this->max - $this->min);
				$image->line($lastX, $lastY, $x, $y, NImage::rgb(0xff,0,0));
				$image->line($lastX, $lastD, $x, $d, NImage::rgb(0,0xff,0));
				$lastD = $d;
			}
			$last = $this->values[$i];
			$lastX = $x;
			$lastY = $y;
			if ($peak === NULL || $this->values[$i] > $peak)
				$peak = $this->values[$i];
		}
		$image->string(1, 0, 0, $peak, NImage::rgb(0,0,0));
		if ($path) {
			$image->save($path, 100, NImage::PNG);
 			$this->values = array();	
		} else
			$image->send(NImage::PNG);
	}

}